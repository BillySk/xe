import React from 'react';
import './App.css';
import TextArea from './TextArea.js';
import xe from './xe.jpg'

class App extends React.Component {
  render() {
    return (
      <div className="main-wrapper">
        <div className="row h-50" id="wrapper" >
          <div className="h-100 col-md-2 col-lg-2 d-none d-sm-none d-md-none d-lg-block" >
            <div className="banner h-100" data-equalizer-watch>
              <div className="bannerText">Baner space</div>
            </div>
          </div>
          <div className="col-4 col-lg-2 col-md-4 col-sm-4 my-panel">
            <img className="logo" src={xe}/>
          </div>
          <div  className="col-lg-8 main-content" data-equalizer-watch>
            <p className="react-autosuggest__suggestions-container">
              What place are you looking for? 
            </p>
            <div class="text-area">
              <TextArea/>
            </div>
          </div>
          <div className="h-100 m-auto d-none d-md-block col-md-10 d-lg-none .d-xl-none" >
            <div className="banner h-100" data-equalizer-watch>
              <div className="bannerText">Baner space</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
