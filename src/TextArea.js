import React from 'react';
import Autosuggest from 'react-autosuggest';
import { isMobile } from "react-device-detect";

class TextArea extends React.Component {
    constructor() {
        super();
    
        // Autosuggest is a controlled component.
        // This means that you need to provide an input value
        // and an onChange handler that updates this value (see below).
        // Suggestions also need to be provided to the Autosuggest,
        // and they are initially empty because the Autosuggest is closed.
        this.state = {
          value: '',
          suggestions: []
        };
    }

     // Teach Autosuggest how to calculate suggestions for any given input value.
    getSuggestions(value) {
        const inputValue = value.trim().toLowerCase();
        if (inputValue.length <= 1) {
            return;
        }
        const language = (navigator.language || navigator.userLanguage).split("-")[0];
        const limit = (isMobile) ? 10 : 20;
        fetch('http://35.180.182.8/search?keywords=' + inputValue + '&language=' + language + '&limit=' + limit)
        .then(response => response.json())
        .then(data => this.setState({ suggestions: data && data.entries || [] }))
    };
    
    // When suggestion is clicked, Autosuggest needs to populate the input
    // based on the clicked suggestion. Teach Autosuggest how to calculate the
    // input value for every given suggestion.
    getSuggestionValue = suggestion => suggestion.name;
    
    // Use your imagination to render suggestions.
    renderSuggestion = suggestion => (
        <div>
        {suggestion.name}
        </div>
    );

    onChange = (event, { newValue }) => {
        this.suggestionSelected = '';
        this.setState({
          value: newValue
        });
    };
    timeout = null;
    
    // Autosuggest will call this function every time you need to update suggestions.
    // You already implemented this logic above, so just use it.
    onSuggestionsFetchRequested = ({ value }) => {
        if(this.suggestionSelected === value) {
            return;
        }
        clearTimeout(this.timeout);
        this.timeout = setTimeout(
            function() {
                this.getSuggestions(value);
                this.setState({position: 1});
            }
            .bind(this),
            600
        );
    };
    
    // Autosuggest will call this function every time you need to clear suggestions.
    onSuggestionsClearRequested = () => {
        this.setState({
        suggestions: []
        });
    };
    
    suggestionSelected='';

    onSuggestionSelected = (event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) => {
        this.suggestionSelected = suggestionValue;
    }

    redirectToGoogle(value) {
        window.location.href ='https://www.google.com/search?q=' +value+ '&oq=' +value;
    }

    render() {
        const { value, suggestions } = this.state;
    
        // Autosuggest will pass through all these props to the input.
        const inputProps = {
          placeholder: 'Type a location',
          value,
          onChange: this.onChange
        };
    
        // Finally, render it!
        return (
        <div>
            <Autosuggest
                suggestions={suggestions}
                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                getSuggestionValue={this.getSuggestionValue}
                renderSuggestion={this.renderSuggestion}
                onSuggestionSelected={this.onSuggestionSelected}
                inputProps={inputProps}
            />          
            <button className="search-button" onClick={this.redirectToGoogle.bind(this, inputProps.value)} disabled={!this.suggestionSelected}>Click to search</button>
        </div>
        );
      }
}

export default TextArea;